chidori.controller("controller",['$scope','$route', '$routeParams', '$location',function($scope,$route, $routeParams,$location){

	$scope.author="Shadow Of Susanoo";
	$scope.description="Multiple Choice Question (MCQ) asking and answering site."
	$scope.title="LazyThoughts";
	$scope.forge_label="Forge";
	$scope.factory_label="Factory";
	$scope.repository_label="Repository";
	$scope.profile_label="Profile";
	$scope.faq_label="F.A.Q";
	$scope.logout_label="Log Out";
	$scope.login_label="Log in";
	$scope.signup_label="Sign up";
	$scope.faq_list=FAQ;
	$scope.user=User;
	$scope.question=Question;
	$scope.repo=Repo;

	$scope.$on('$routeChangeStart', function(next,current) {
       	show("#view-transition-loader");
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
       	hide("#view-transition-loader");
	});

	$scope.$on('$routeChangeError', function(next,current) {
       	hide("#view-transition-loader");
	});

	$scope.deleteQuestion=function(val){
		show($("#q_del_warning_"+val));
	}

	$scope.dontDeleteQ=function(val){
		hide($("#q_del_warning_"+val));
	}

	$scope.hideMainPreloader=function(){
		hide("#main_preloader");
	}

	$scope.deleteQ=function(val){
		var data={
			"sessionId":$scope.user.sessionId,
			"questionId":$scope.repo[val].questionId
		};
		var pl="#main_preloader";
		show(pl);
		post_json(url_question_delete,data,function(response){
			hide(pl);
			if(response.sessionId=="-1"){
				$scope.invalidSession();
			}
			if(response.success){
				toast("Question deleted!");
				if(Repo!=null){
					Repo.splice(val,1);
					$scope.repo=Repo;
				}
				$scope.$apply();
			}else{
				toast("Unable to delete question!");
			}
		},function(error,response){
			hide(pl);
			toast("Unable to connect to server!");
		});
	}

	$scope.analyzer=function(val){
		if($scope.repo[val].chart.totalAns==0){
			toast("This question has no answer , therefore there is nothing to analyze.");
			return;
		}
		show($("#analysis_"+val));
		hide($("#analyzer_btns_"+val));
		show($("#deanalyzer_btns_"+val));
	}

	$scope.deanalyzer=function(val){
		hide($("#analysis_"+val));
		show($("#analyzer_btns_"+val));
		hide($("#deanalyzer_btns_"+val));
	}

	$scope.analysisType=function(val){
		if($("#cb_analysis_type_"+val).is(":checked")){
			hide($("#textual_"+val));
			show($("#graphical_"+val));
		}else{
			show($("#textual_"+val));
			hide($("#graphical_"+val));
		}
	}

	$scope.graphicalAnalysisType=function(val){
		if($("#cb_graphical_analysis_type_"+val).is(":checked")){
			hide($("#bar_graph_"+val));
			show($("#graph_"+val));
		}else{
			show($("#bar_graph_"+val));
			hide($("#graph_"+val));
		}
	}

	$scope.checkRepo=function(){
		if($scope.repo==null || $scope.repo.length==0){
			show($("#no_q"));
		}else{
			hide($("#no_q"));
		}
	}

	$scope.getRepo=function(){
		if($scope.user.sessionId=="-1"){
			$scope.terminateSession();
			return;
		}
		$scope.checkRepo();
		var data={
			"sessionId":$scope.user.sessionId
		};
		var pl="#main_preloader";
		show(pl);
		post_json(url_question_repo,data,function(response){
			Repo=response;
			hide(pl);
			if(Repo!=null){
				for(var i=0;i<Repo.length/2;i++){
					var t=Repo[i];
					Repo[i]=Repo[Repo.length-i-1];
					Repo[Repo.length-i-1]=t;
				}
				for(var i=0;i<Repo.length;i++){
					Repo[i].multipleAnswersLabel="single choice answer";
					Repo[i].clr="teal";
					Repo[i].time=dateFromMs(Repo[i].timestamp);
					if(Repo[i].multipleAnswers){
						Repo[i].multipleAnswersLabel="multiple choice answer";
						Repo[i].clr="red";
					}
					var simpleAnswers=new Array(Repo[i].options.length);
					for(var j=0;j<Repo[i].options.length;j++){
						Repo[i].options[j]="Option #"+(j+1)+" : "+Repo[i].options[j];
						if(Repo[i].answers.hasOwnProperty(j)){
							simpleAnswers[j]=Repo[i].answers[j];
						}else{
							simpleAnswers[j]=0;
						}
					}
					Repo[i].ques=$.trim(Repo[i].question);
					Repo[i].question=Repo[i].ques.split("\n");
					var chart={
						"options":{
							"title":Repo[i].ques,
							"height":400,
							"width":500
						}
					}
					chart.totalAns=0;
					chart.data=[];
					for(var j=0;j<simpleAnswers.length;j++){
						chart.totalAns+=simpleAnswers[j];
					}
					for(var j=0;j<simpleAnswers.length;j++){
						chart.data.push([Repo[i].options[j],simpleAnswers[j],((simpleAnswers[j]*100.0)/chart.totalAns).toFixed(2)]);
					}
					if(chart.totalAns==0){
						chart.ansCountClr="blue-grey";
						chart.totalAnsStatement="no answer";
					}else{
						if(chart.totalAns==1){
							chart.ansCountClr="green";
							chart.totalAnsStatement="1 answer";
						}else{
							chart.ansCountClr="purple";
							chart.totalAnsStatement=chart.totalAns+" answers";
						}
					}
					Repo[i].chart=chart;
					Repo[i].simpleAnswers=simpleAnswers;				
				}
				$scope.repo=Repo;
				$scope.$apply();
				$scope.checkRepo();
				for(var i=0;i<Repo.length;i++){
					var data=new google.visualization.DataTable();
					data.addColumn('string', 'Options');
	        		data.addColumn('number', 'Frequency');
	        		var d=[];
	        		for(var k=0;k<Repo[i].chart.data.length;k++){
	        			var tmp=Repo[i].chart.data[k];
	        			d.push([tmp[0],tmp[1]]);
	        		}
	        		data.addRows(d);
	        		var options = Repo[i].chart.options;
	        		chart = new google.visualization.PieChart($("#graph_"+i)[0]);
	        		chart.draw(data, options);
	        		var barchart = new google.visualization.BarChart($("#bar_graph_"+i)[0]);
	        		barchart.draw(data,options);
        		}
			}else{
				toast("Unable to load repository! Please try again.");
			}
		},function(error,response){
			hide(pl);
			toast("Unable to connect to server!");
		});
	}

	$scope.answerQuestion=function(){
		if($scope.question!=null){
			var Q=$scope.question;
			var answers=[];
			for(var i=0;i<Q.options.length;i++){
				var id="#option_"+i;
				if($(id).is(':checked')){
					answers.push(i);
				}
			}
			if(answers.length==0){
				toast("Please select an option before submitting the answer.");
				return;
			}
			if(!Q.multipleAnswers && answers.length>1){
				toast("You can select only one option as an answer to this question.");
				return;
			}
			var data={
				"answer":answers,
				"questionId":Q.questionId,
				"sessionId":$scope.user.sessionId
			};
			var pl="#main_preloader";
			show(pl);
			post_json(url_answer,data,
				function(response){
					var answerEle=response[0];
					var questionEle=response[1];
					hide(pl);
					if(answerEle.sessionId=="-1"){
						$scope.invalidSession();
					}
					if(answerEle.success){
						$scope.resetOptionTicks();
						var sum=0;
						var ansOpts=[];
						for(var i=0;i<questionEle.options.length;i++){
							var statement="Option #"+(i+1)+" : "+questionEle.options[i];
							var a=0;
							if(questionEle.answers!=null && questionEle.answers.hasOwnProperty(i)){
								a=questionEle.answers[i];
							}
							sum+=a;
							var freq=a;
							ansOpts.push({"statement":statement,"freq":freq,"per":0});
						}
						for(var i=0;i<ansOpts.length;i++){
							if(sum!=0)
								ansOpts[i].per=((ansOpts[i].freq*100.0)/sum).toFixed(2);
							ansOpts[i].clr=$scope.randomColor();
						}
						if($scope.question!=null && 
							$scope.question.questionId==questionEle.questionId)
							$scope.question.options=ansOpts;
						$scope.$apply();
						toast("Answer updated!");
						/*
						for(var i=0;i<answerEle.answer.length;i++){
							$("#option_"+answerEle.answer[i])[0].checked=true;
						}
						*/
					}else{
						toast("Unable to record answer!");
					}
				},
				function(error,response){
					hide(pl);
					toast("Unable to connect to server!");
				}
			);
		}else{
			toast("Oops! It looks like something went wrong :/");
		}
	}

	$scope.invalidSession=function(){
		//toast("Something went wrong :/");
		//$scope.terminateSession();
		//$scope.$apply();
	}

	$scope.resetOptionTicks=function(){
		if($scope.question!=null){
			for(var i=0;i<$scope.question.options.length;i++){
				var id="#option_"+i;
				$(id).attr('checked',false);
			}
		}
	}

	$scope.getAnswerableQuestion=function(val){
		if($scope.user.sessionId=="-1"){
			$scope.terminateSession();
			return;
		}
		var noMoreDiv="#no_more_q_error";
		var errorDiv="#question_loading_error";
		var answerDiv="#answer_maker";
		var pl="#main_preloader";
		hide(noMoreDiv);
		hide(errorDiv);
		hide(answerDiv);
		show(pl);
		var url=url_question_latest;
		if(val=='prev'){
			url=url_question_prev;
		}else if(val=='next'){
			url=url_question_next;
		}else if(val=='curr'){
			if($scope.questionId!=null){
				$scope.questionId++;
				url=url_question_prev;
			}
		}
		var data={
			"sessionId":$scope.user.sessionId,
			"questionId":$scope.questionId,
			"category":$scope.category
		};
		if($scope.questionId==null){
			url=url_question_latest;
		}
		post_json(url,data,
			function(response){
				if(response.sessionId=="-1"){
					$scope.invalidSession();
				}
				if(response.questionId!=-1){
					data={
						"sessionId":$scope.user.sessionId,
						"questionId":response.questionId
					};
					post_json(url_question,data,function(response){
						if(response.sessionId=="-1"){
							$scope.invalidSession();
						}
						if(response.success){
							hide(pl);
							Question=response;
							var sum=0;
							var ansOpts=[];
							for(var i=0;i<Question.options.length;i++){
								var statement="Option #"+(i+1)+" : "+Question.options[i];
								var a=0;
								if(Question.answers!=null && Question.answers.hasOwnProperty(i)){
									a=Question.answers[i];
								}
								sum+=a;
								var freq=a;
								ansOpts.push({"statement":statement,"freq":freq,"per":0});
							}
							for(var i=0;i<ansOpts.length;i++){
								if(sum!=0)
									ansOpts[i].per=((ansOpts[i].freq*100.0)/sum).toFixed(2);
								ansOpts[i].clr=$scope.randomColor();
							}
							Question.options=ansOpts;
							Question.time=dateFromMs(Question.timestamp);
							Question.multipleAnswersLabel="multiple choice answer";
							Question.clr="red";
							if(!Question.multipleAnswers){
								Question.multipleAnswersLabel="single choice answer";
								Question.clr="teal";
							}
							Question.username="...";
							Question.usernameClr="blue-grey";
							Question.question=$.trim(Question.question).split("\n");
							$scope.question=Question;
							hide(errorDiv);
							hide(noMoreDiv);
							show(answerDiv);
							$scope.questionId=response.questionId;
							$scope.$apply();
							data={
								"sessionId":$scope.user.sessionId,
								"userId":response.userId
							};
							post_json(url_profile_id,data,function(response){
								if(response.success){
									Question.username=response.username;
									Question.usernameClr="green";
									$scope.question=Question;
									$scope.$apply();
								}else{
									toast("Unable to fetch username!");
								}
							},function(error,response){
								toast("Please check your internet connection.")
							});
						}else{
							hide(pl);
							show(errorDiv);
						}
					},function(error,response){
						hide(pl);
						show(errorDiv);
					});
				}else{
					hide(pl);
					show(noMoreDiv);
					if($scope.questionId!=null){
						if(url==url_question_next){
							$scope.questionId+=1;
						}else if(url=url_question_prev){
							$scope.questionId-=1;
						}
					}
					$scope.$apply();
				}
			},
			function(error,response){
				hide(pl);
				show(errorDiv);
			}
		);
	}

	$scope.randomColor=function(){
		var c=['red','green','orange','blue','pink','purple','teal','lime','brown','cyan'];
		var i=Math.floor((Math.random()*10));
		return c[i];
	}

	$scope.showChangeUsername=function(){
		show("#username_change");
	}

	$scope.createQuestion=function(){
		var question=$.trim($("#question").val());
		var options=[];
		$("#question_error").text("");
		hide("#question_error");
		for(var i=0;i<$scope.range_num;i++){
			options.push($.trim($("#option_"+i).val()));
			hide("#option_error_"+i);
			$("#option_error_"+i).text("");
		}
		var ret=false;
		if(question==""){
			show($("#question_error"));
			$("#question_error").text("Enter a question.");
			ret=true;
		}
		for(var i=0;i<options.length;i++){
			if(options[i]==""){
				show("#option_error_"+i);
				$("#option_error_"+i).text("An option cannot be empty!");
				ret=true;
			}
		}
		if(ret){
			return;
		}
		show("#create_question_preloader");
		var restricted=false;
		var multipleAnswers=$("#cb_multiple_options").is(':checked');
		var category=$("#select_category").val();
		var data={
			"question":question,
			"options":options,
			"restricted":restricted,
			"category":category,
			"multipleAnswers":multipleAnswers,
			"sessionId":$scope.user.sessionId
		};
		post_json(url_question_create,data,
			function(response){
				hide("#create_question_preloader");
				if(response.sessionId=="-1"){
					$scope.invalidSession();
				}
				if(response.success){
					$("#question_error").text("");
					hide("#question_error");
					$("#question").val("");
					for(var i=0;i<options.length;i++){
						hide("#option_error_"+i);
						$("#option_error_"+i).text("");
						$("#option_"+i).val("");
					}
					$("#cb_multiple_options")[0].checked=false;
					$scope.resetOptionCount();
					$scope.$apply();
					toast("Question posted!");
				}else{
					toast("Unable to post question! Please try again.");
				}
			},
			function(error,response){
				toast("Unable to connect to server!");
				hide("#create_question_preloader");
			}
		);
	}

	$scope.hideChangeUsername=function(){
		hide("#username_change");
	}

	$scope.showAccountDelete=function(){
		show("#account_delete");
	}

	$scope.hideAccountDelete=function(){
		hide("#account_delete");
	}

	$scope.showChangePassword=function(){
		show("#password_change");
	}

	$scope.hideChangePassword=function(){
		hide("#password_change");
	}

	$scope.getOptionCount = function() {
    	var arr=new Array($scope.range_num);  
    	for(var i=0;i<arr.length;i++){
    		arr[i]=i;
    	} 
    	return arr;
	}

	$scope.deactivateAllTabs=function(){
		$("#repo-tab").removeClass('active');
		$("#factory-tab").removeClass('active');
		$("#forge-tab").removeClass('active');
	}

	$scope.activateTab=function(val){
		$scope.deactivateAllTabs();
		$(val).addClass('active');
	}

	$scope.resetOptionCount = function() {
    	$scope.range_num=2;  
	}

	$scope.decOptionCount = function() {
		if($scope.range_num>2){
    		$scope.range_num--;  
		}else{
			toast("You need to provide at least 2 options!");
		}
	}

	$scope.incOptionCount = function() {
    	$scope.range_num++;  
	}

	$scope.changeUsername=function(){
		hide("#change_username_error");
		$("#change_username_error").text("");
		$scope.username=$.trim($scope.username);
		var ret=false;
		if($scope.username==""){
			ret=true;
			$("#change_username_error").text("Please enter new username.");
			show("#change_username_error");
		}
		if(ret){
			return;
		}
		show("#change_username_preloader");
		var data={
			"sessionId":$scope.user.sessionId,
			"newUsername":{"value":$scope.username}
		};
		post_json(url_change_username,data,
			function(response){
				hide("#change_username_preloader");
				if(response.sessionId=="-1"){
					$scope.invalidSession();
				}
				if(response.success){
					User.setUsername(response.username);
					$scope.$apply();
					toast("Username changed to '"+response.username+"'");
				}else if(!response.newUsername.valid){
					var err="";
					var n=response.newUsername;
					if(n.tooLong){
						err+="Username too long, maximum length is 30 characters.";
					}else if(n.tooShort){
						err+="Username too short, minimum length is 3 characters.";
					}
					if(n.invalidCharacters){
						if(err!="")err+="<br/>";
						err+="Username has invalid characters, only alphanumeric characters, '.' and '_' are allowed.";
					}
					$("#change_username_error").text(err);
					$("#change_username_error").removeClass("hidden");
				}else if(!response.uniqueUsername){
					$("#change_username_error").text("The username '"+$scope.username+"' is already in use! Please try again with a different username.");
					$("#change_username_error").removeClass("hidden");
				}
			},
			function(error,response){
				$("#change_username_error").text("Unable to connect to server!");
				$("#change_username_error").removeClass("hidden");
				hide("#change_username_preloader");
			}
		);
	}

	$scope.changePassword=function(){
		hide("#change_new_password_error");
		$("#change_new_password_error").text("");
		hide("#change_old_password_error");
		$("#change_old_password_error").text("");
		$scope.old_password=$.trim($scope.old_password);
		$scope.new_password=$.trim($scope.new_password);
		var ret=false;
		if($scope.old_password==""){
			ret=true;
			$("#change_old_password_error").text("Please enter old password.");
			show("#change_old_password_error");
		}
		if($scope.new_password==""){
			ret=true;
			$("#change_new_password_error").text("Please enter new password.");
			show("#change_new_password_error");
		}
		if(ret){
			return;
		}
		show("#change_password_preloader");
		var data={
			"sessionId":$scope.user.sessionId,
			"oldPassword":$scope.old_password,
			"newPassword":{"value":$scope.new_password}
		};
		post_json(url_change_password,data,
			function(response){
				hide("#change_password_preloader");
				if(response.sessionId=="-1"){
					$scope.invalidSession();
				}
				if(response.success){
					toast("Password changed!");
					$scope.old_password="";
					$scope.new_password="";
					$scope.$apply();
				}else if(!response.newPassword.valid){
					var err="";
					var n=response.newPassword;
					if(n.tooLong){
						err+="Password too long, maximum length is 30 characters.";
					}else if(n.tooShort){
						err+="Password too short, minimum length is 3 characters.";
					}
					if(n.invalidCharacters){
						if(err!="")err+="<br/>";
						err+="Password has invalid characters, only alphanumeric characters, '.' , '_' and '@' are allowed.";
					}
					$("#change_new_password_error").text(err);
					$("#change_new_password_error").removeClass("hidden");
				}else if(response.wrongPassword){
					$("#change_old_password_error").text("Incorrect old password! Are you really "+$scope.user.username+" ?");
					$("#change_old_password_error").removeClass("hidden");
				}
			},
			function(error,response){
				$("#change_new_password_error").text("Unable to connect to server!");
				$("#change_new_password_error").removeClass("hidden");
				hide("#change_password_preloader");
			}
		);
	}

	$scope.deleteAccount=function(){
		var errorDiv="#del_ac_error";
		var preloaderDiv="#del_ac_preloader";
		hide(errorDiv);
		$(errorDiv).text("");
		$scope.del_ac_password=$.trim($scope.del_ac_password);
		var ret=false;
		if($scope.del_ac_password==""){
			ret=true;
			$(errorDiv).text("Please enter password.");
			show(errorDiv);
		}
		if(ret){
			return;
		}
		show(preloaderDiv);
		var data={
			"username":{"value":$scope.user.username},
			"password":{"value":$scope.del_ac_password}
		}
		post_json(url_login,data,
			function(response){
				if(response.sessionId=="-1"){
					hide(preloaderDiv);
					$(errorDiv).text("Invalid password!");
					$(errorDiv).removeClass("hidden");
				}else{
					post_json(url_delete_account,{"sessionId":User.sessionId},
						function(response){
							hide(preloaderDiv);
							if(response.sessionId=="-1"){
								$scope.invalidSession();
							}
							if(response.success){
								toast("Your account has been deleted!");
								$scope.terminateSession();
								$scope.$apply();
							}else{
								$(errorDiv).text("Unable to delete account.");
								$(errorDiv).removeClass("hidden");
							}
						},function(error,response){
							$(errorDiv).text("Unable to connect to server!");
							$(errorDiv).removeClass("hidden");
							hide(preloaderDiv);
						});
				}
			},
			function(error,response){
				$(errorDiv).text("Unable to connect to server!");
				$(errorDiv).removeClass("hidden");
				hide(preloaderDiv);
			}
		);
	}

	$scope.checkForActiveSession=function(){
		if($scope.user.isActiveSession()){
			$scope.switchView("/factory");
			show("#top-bar");
		}
		$scope.activateTab('#factory-tab');
	}

	$scope.checkForInactiveSession=function(){
		if(!$scope.user.isActiveSession()){
			$scope.terminateSession();
		}else{
			show("#top-bar");
		}
	}

	$scope.terminateSession=function(){
		User.logout();
		$scope.switchView('/login');
		hide("#top-bar");
	}

	$scope.logout=function(){
		toast("Logging you out...");
		var data={
			"sessionId":$scope.user.sessionId
		};
		show("#main_preloader");
		post_json(url_logout,data,
			function(response){
				hide("#main_preloader");
				toast("You have been logged out.");
				User.logout();
				$scope.switchView('/login');
				hide("#top-bar");
				$scope.$apply();
			},
			function(error,response){
				toast("Unable to connect to server!");
				hide("#main_preloader");
			}
		);
	}

	$scope.switchView=function(view){
		$location.path(view);
	}

	$scope.signup=function(){
		$("#signup_error").addClass("hidden");
		$("#signup_username_error").addClass("hidden");
		$("#signup_password_error").addClass("hidden");
		$("#signup_dob_error").addClass("hidden");
		$("#signup_error").text("");
		$("#signup_username_error").text("");
		$("#signup_password_error").text("");
		$("#signup_dob_error").text("");
		$scope.username=$.trim($scope.username);
		$scope.password=$.trim($scope.password);
		var dob=$.trim($("#signup_dob").val());
		var password_2=$.trim($scope.password_2);
		var ret=false;
		if($scope.password!=password_2){
			ret=true;
			$("#signup_password_error").text("Passwords don't match.");
			$("#signup_password_error").removeClass("hidden");
		}
		if(ret){
			return;
		}
		if($scope.username==""){
			ret=true;
			$("#signup_username_error").text("Enter username.");
			$("#signup_username_error").removeClass("hidden");
		}
		if($scope.password==""){
			ret=true;
			$("#signup_password_error").text("Enter password.");
			$("#signup_password_error").removeClass("hidden");
		}
		if(dob==""){
			ret=true;
			$("#signup_dob_error").text("Enter your date of birth.");
			$("#signup_dob_error").removeClass("hidden");
		}
		if(ret){
			return;
		}
		$("#signup_preloader").removeClass("hidden");
		var data={
			"username":{"value":$scope.username},
			"password":{"value":$scope.password},
			"dob":dob
		}
		post_json(url_signup,data,
			function(response){
				$("#signup_preloader").addClass("hidden");
				if(response.sessionId=="-1"){
					var creds=true;
					if(!response.username.valid){
						creds=false;
						var err="";
						var n=response.username;
						if(n.tooLong){
							err+="Username too long, maximum length is 30 characters.";
						}else if(n.tooShort){
							err+="Username too short, minimum length is 3 characters.";
						}
						if(n.invalidCharacters){
							if(err!="")err+="<br/>";
							err+="Username has invalid characters, only alphanumeric characters, '.' and '_' are allowed.";
						}
						$("#signup_username_error").text(err);
						$("#signup_username_error").removeClass("hidden");
					}
					if(!response.password.valid){
						creds=false;
						var err="";
						var n=response.password;
						if(n.tooLong){
							err+="Password too long, maximum length is 30 characters.";
						}else if(n.tooShort){
							err+="Password too short, minimum length is 3 characters.";
						}
						if(n.invalidCharacters){
							if(err!="")err+="<br/>";
							err+="Password has invalid characters, only alphanumeric characters, '.' , '_' and '@' are allowed.";
						}
						$("#signup_password_error").text(err);
						$("#signup_password_error").removeClass("hidden");
					}
					if(creds && !response.uniqueUsername){
						$("#signup_username_error").text("The username '"+$scope.username+"' is already in use! Please try again with a different username.");
						$("#signup_username_error").removeClass("hidden");
					}
				}else{
					User.setUser(response.sessionId,response.userId,response.username.value,response.dob,response.signupTimestamp);
					$scope.switchView("/factory");
					$scope.$apply();
					show("#top-bar");
				}
			},
			function(error,response){
				$("#signup_error").text("Unable to connect to server!");
				$("#signup_error").removeClass("hidden");
				$("#signup_preloader").addClass("hidden");
			}
		);
	}

	$scope.login=function(){
		$("#login_error").addClass("hidden");
		$("#login_username_error").addClass("hidden");
		$("#login_password_error").addClass("hidden");
		$("#login_error").text("");
		$("#login_username_error").text("");
		$("#login_password_error").text("");
		$scope.username=$.trim($scope.username);
		$scope.password=$.trim($scope.password);
		var ret=false;
		if($scope.username==""){
			ret=true;
			$("#login_username_error").text("Enter username.");
			$("#login_username_error").removeClass("hidden");
		}
		if($scope.password==""){
			ret=true;
			$("#login_password_error").text("Enter password.");
			$("#login_password_error").removeClass("hidden");
		}
		if(ret){
			return;
		}
		$("#login_preloader").removeClass("hidden");
		var data={
			"username":{"value":$scope.username},
			"password":{"value":$scope.password}
		}
		post_json(url_login,data,
			function(response){
				$("#login_preloader").addClass("hidden");
				if(response.sessionId=="-1"){
					$("#login_error").text("Invalid username/password combination.");
					$("#login_error").removeClass("hidden");
				}else{
					User.setUser(response.sessionId,response.userId,response.username.value,response.dob,response.signupTimestamp);
					$scope.switchView("/factory");
					$scope.$apply();
					show("#top-bar");
				}
			},
			function(error,response){
				$("#login_error").text("Unable to connect to server!");
				$("#login_error").removeClass("hidden");
				$("#login_preloader").addClass("hidden");
			}
		);
	}

}]);