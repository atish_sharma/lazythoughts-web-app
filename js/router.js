chidori.config(function($routeProvider){
	$routeProvider
    .when('/', {
    	templateUrl : 'views/login.html'
        ,controller  : 'controller'
    })
    .when('/login', {
    	templateUrl : 'views/login.html'
        ,controller  : 'controller'
    })
    .when('/signup', {
    	templateUrl : 'views/signup.html'
        ,controller  : 'controller'
    })
    .when('/factory', {
        templateUrl : 'views/factory.html'
        ,controller  : 'controller'
    })
    .when('/profile', {
        templateUrl : 'views/profile.html'
        ,controller  : 'controller'
    })
    .when('/faq', {
        templateUrl : 'views/faq.html'
        ,controller  : 'controller'
    })
    .when('/forge', {
        templateUrl : 'views/forge.html'
        ,controller  : 'controller'
    })
    .when('/repo', {
        templateUrl : 'views/repository.html'
        ,controller  : 'controller'
    })
    .otherwise({
    	redirectTo : '/'
    });
});