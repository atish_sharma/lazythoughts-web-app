var url_site="http://localhost:8080/Chidori/";
//var url_site="http://chidori-shadowofsusanoo.rhcloud.com/";
var url_login=url_site+"log/in";
var url_signup=url_site+"sign/up";
var url_logout=url_site+"log/out";
var url_profile_id=url_site+"profile/id";
var url_change_username=url_site+"profile/change/username";
var url_change_password=url_site+"profile/change/password";
var url_delete_account=url_site+"profile/delete";
var url_get_user_by_id=url_site+"profile/id";
var url_get_user_by_username=url_site+"profile/username";
var url_question_create=url_site+"question/create";
var url_question_latest=url_site+"question/id/latest";
var url_question_next=url_site+"question/id/next";
var url_question_prev=url_site+"question/id/previous";
var url_question=url_site+"question/id";
var url_question_delete=url_site+"question/delete";
var url_question_repo=url_site+"question/repository";
var url_answer=url_site+"answer/id";

var Question=null;
var Repo=null;

var FAQ=[
        {
            'Q':'What is this?',
            'A':'LazyThoughts is a simple MCQ asking and answering web application.'
        },
        {
            'Q':'What is a MCQ?',
            'A':'MCQ stands for multiple choice question. Such questions have options specified as their possible answer.'
        },
        {
            'Q':'Can I answer a question more than once?',
            'A':'Yes.'
        },
        {
            'Q':'What happens to my previous answer after I answer a question again?',
            'A':'Your previous answer will be deleted and your latest answer will be considered as your only answer to the question.'
        },
        {
            'Q':'What is "factory"?',
            'A':'It is a metaphor for a place where all the answers to your questions are produced.'
        },
        {
            'Q':'What is "repository"?',
            'A':'It is a metaphor for a place where all your questions are safely stored.'
        },
        {
            'Q':'What is "forge"?',
            'A':'It is a metaphor for a place where all your questions are created.'
        },
        {
            'Q':'How do I change my date of birth?',
            'A':'You cannot change your date of birth after sign up.'
        },
        {
            'Q':'Can I see who all have answered my question?',
            'A':'No.'
        },
        {
            'Q':'What is "multiple choice answer"?',
            'A':'Questions for which more than one option can be selected as the answer are labelled as "multiple choice answer".'
        },
        {
            'Q':'What is "single choice answer"?',
            'A':'Questions for which only one option can be selected as an answer are labelled as "single choice answer".'
        }
    ];

var User={

    username:"",
    sessionId:"-1",
    userId:-1,
    dob:"",
    timestamp:0,//signup timestamp
    signupDate:null,

    setUsername:function(val){
        this.username=val;
        this.write();
    },

    setUserId:function(val){
        this.userId=val;
        this.write();
    },

    setSessionId:function(val){
         this.sessionId=val;
        this.write();
    },

    setDob:function(dob){
        this.dob=dob;
    },

    setTimestamp:function(timestamp){
        this.timestamp=timestamp;
    },

    setUser:function(session,id,username,dob,timestamp){
        this.setUsername(username);
        this.setSessionId(session);
        this.setUserId(id);
        this.setDob(dob);
        this.setTimestamp(timestamp);
        this.write();
    },

    logout:function(){
        this.setUser("-1",-1,"");
        Question=null;
        Repo=null;
        this.del();
    },

    write:function(){
        localStorage.ChidoriUser=JSON.stringify(this);
        this.createSignupDate();
    },

    read:function(){
        if(localStorage.ChidoriUser!=null){
            var user=JSON.parse(localStorage.ChidoriUser);
            this.username=user.username;
            this.sessionId=user.sessionId;
            this.userId=user.userId;
            this.dob=user.dob;
            this.timestamp=user.timestamp;
            this.createSignupDate();
        }
    },

    del:function(){
        delete localStorage.ChidoriUser;
    },

    createSignupDate:function(){
        this.signupDate=dateFromMs(this.timestamp);
    },

    isActiveSession:function(){
        this.read();
        return this.sessionId!="-1";
    }

};

$(document).ready(function(){
	init();
});

function dateFromMs(ms){
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
    var date = new Date(ms);
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var hours = date.getHours();
    var meridian=hours>=12?'PM':'AM';
    if(hours!=12){
        hours%=12;
    }
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    minutes=minutes.toString();
    hours=hours.toString();
    if(minutes.length<2){
        minutes='0'+minutes;
    }
    if(hours.length<2){
        hours='0'+hours;
    }
    return day + ' ' + monthNames[monthIndex] + ', ' + year + ' @ ' +hours + ':' + minutes +' '+ meridian ;
}

function init(){
	$("#profile-dropdown-button").dropdown();
}

function hide(o){
	$(o).addClass('hidden');
}

function show(o){
	$(o).removeClass('hidden');
}  

function post_json(url,json,success_function,error_function){
	$.ajax({
        url: url,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(json),
        dataType: 'json',
        async: true,
        success: success_function,
        error: error_function
    });
}

function toast(val){
    Materialize.toast(val, 3000, 'rounded');
}